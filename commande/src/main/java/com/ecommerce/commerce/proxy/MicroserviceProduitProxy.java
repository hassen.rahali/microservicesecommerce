package com.ecommerce.commerce.proxy;


import com.ecommerce.commerce.beans.ProduitBean;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@FeignClient(name = "produitMicroservice", url = "localhost:81")
public interface MicroserviceProduitProxy {

    @GetMapping(value="products/{id}")
    ResponseEntity<ProduitBean> getProduct (@PathVariable Long  id) throws ResourceNotFoundException ;

    @GetMapping(value="products")
    List<ProduitBean> getAllProducts ();

    @PostMapping(value = "products")
    ResponseEntity<ProduitBean> addproduct (@RequestBody ProduitBean prod);

    @GetMapping(value="products/{id}")
    void deleteProduct(@PathVariable Long id) throws ResponseStatusException;

    }