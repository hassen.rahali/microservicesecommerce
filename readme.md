##### E-Commerce web Application
This work was done by : 
- Amani Tokebri
- Hassen Rahali 
- Ahmed Werghi
- Annouar Bouakkez 
##### We have developed this web application using **Microservice** Architecture. So we have dived the project into 5 sub projects:
##### Development phase
- **Product Microservice**: Responsible for all operations that includes products.
- **Commmand Microservice**: Responsible for all operations that includes commands.
- **Client Microsevrice**: Responsible for all operations that includes client.
- **Payment Microsevice**: Responsible for all operations of payment.
- **Config Microservice**: Responsible for all operations that includes all the configurations of all these microservices.


##### _You can find the documentation of every micro service in a readme in the microservice directory_

##### Dockerzing phase
After developing the microservices we have created a docker image with every micro service and pushed them to hub.docker.
_PLEASE FIND A DOCKER FILE IN EVERY SUB PROJECT DESCRIBING THE BUILD OF THE DOCKER IMAGE_

##### The final step: Deploying the project to an EC2 instance 
All the micro service are deployed in the same instance
Here is links to acces to all these micro services: 

* The first micro service is ProductMicroservice Running on port 81 : [ec2-63-33-210-243.eu-west-1.compute.amazonaws.com:81/products](ec2-63-33-210-243.eu-west-1.compute.amazonaws.com:81/products)
* The second micro service is CommandMicroservice Running on port 82 : [ec2-63-33-210-243.eu-west-1.compute.amazonaws.com:82/commandes](ec2-63-33-210-243.eu-west-1.compute.amazonaws.com:82/commandes)
* The third micro service is PaymentMicroservice Running on port 83 : [ec2-63-33-210-243.eu-west-1.compute.amazonaws.com:83/payment/1](ec2-63-33-210-243.eu-west-1.compute.amazonaws.com:83/payment/1)
* The fourth micro service is ClientMicroservice Running on port 8080 : [ec2-63-33-210-243.eu-west-1.compute.amazonaws.com:8080/client/1](ec2-63-33-210-243.eu-west-1.compute.amazonaws.com:8080/client/1)
* The fifth micro service is Config server Microservice Running on port 84 : [ec2-63-33-210-243.eu-west-1.compute.amazonaws.com:84/actuator](ec2-63-33-210-243.eu-west-1.compute.amazonaws.com:84/actuator)



#### **Client Microservice EndPoints**

These are the requests permited by this MicroService with a bief description.

| Path      |Type| Function                       |
|:--------------|:---|:----------------------------------|
| `/client`      | `POST`|add a client to the data base
| `/client/{id}` |`GET`  |return a JSON object with the client of the ID=id
| `/clientPay/{id}`    |`GET`| allow he client to pay a command having the ID=id
| `/commandes/{id}`     | `GET`|  return a JSON array contain all the commandes done by client having ID=id|
| `/products`      | `GET`|return all products
| `/PasserUneCommande`      | `POST`| pass a command
**_The application is running on port 8080_**



#### **Commande Microservice EndPoints**

These are the requests permited by this MicroService with a bief description.

| Path      |Type| Function                       |
|:--------------|:---|:----------------------------------|
| `/commandes`      | `GET`|returns a json array that contain all the commandes in the database
| `/commandes/{id}`    |`GET`| return the commande with the Id=id in a json format
| `/commandes` |`POST`  |Pass a commande with the json passed in request body
| `/commandes/{id}`     | `DELETE`| Delete the command having an ID=id|
| `/commandes`     | `UPDATE`| Update the command with the json passed in request body

**_The application is running on port 82_**



#### **Product Microservice EndPoints**

These are the requests permited by this MicroService with a bief description.

| Path      |Type| Function                       |
|:--------------|:---|:----------------------------------|
| `/products`      | `GET`|returns a json array that contain all the products in the database
| `/products/{id}`    |`GET`| return the product with the Id=id in a json format
| `/products` |`POST`  |Create a product with the json array passed in request body
| `/products/{id}`     | `DELETE`| Delete the product having an ID=id|

**_The application is running on port 81_**



#### **Payement Microservice EndPoints**

These are the requests permited by this MicroService with a bief description.

| Path      |Type| Function                       |
|:--------------|:---|:----------------------------------|
| `/payment/{commandeId}`      | `GET`|return a payement that client payed

**_The application is running on port 83_**




