package com.monolith.demo.model;


import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
@ToString
public class Commande {

    @Id
    @GeneratedValue
    private Long id;

    private Long idClient;

    private Long idProduct;

    private Date dateCommande ;

    private BigDecimal amount ;

    private int quantity ;

    private boolean isPayed =false  ;

    public void update(Commande commande) {
    }
}
