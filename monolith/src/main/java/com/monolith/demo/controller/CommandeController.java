package com.monolith.demo.controller;


import com.monolith.demo.exceptions.ProductNotAvailableException;
import com.monolith.demo.model.Commande;
import com.monolith.demo.model.Product;
import com.monolith.demo.repository.ProductRepository;
import com.monolith.demo.service.CommandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@RestController
public class CommandeController {

    @Autowired
    CommandeService commandeService;

    @Autowired
    ProductRepository productRepository ;


    @GetMapping(value = "/commandes")
    public List<Commande> allCommandes() {
        return commandeService.findAll();
    }

    @GetMapping(value = "/commandes/{id}")
    public ResponseEntity<Commande> getCommande(@PathVariable Long id) {

        Commande commande = commandeService
                .findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("command not found"));

        return ResponseEntity.ok().body(commande);
    }

    @PutMapping(value = "/commandes")
    public ResponseEntity<Commande> updateCommande(@RequestBody Commande commande) {

        commandeService
                .findById(commande.getId())
                .orElseThrow(()-> new ResourceNotFoundException("commande not found"));

        Commande updatedCommande = commandeService.updateCommande(commande);

        return ResponseEntity.ok().body(updatedCommande);
    }


     @PostMapping (value = "/commandes" )
     public ResponseEntity<Commande> passCommande (@RequestBody Commande commande){

        Product produit = productRepository.findById(commande.getIdProduct()).get();

        //TODO adding client
         // testing if exist in database

        if (produit.getStockquantity() <=0) throw new ProductNotAvailableException("produits not available");

        BigDecimal produitPrice  = produit.getPrice();
        int quantity = commande.getQuantity();
        //calcul command amount
        commande.setAmount(produitPrice.multiply(new BigDecimal(quantity)));

        //set date commande
         commande.setDateCommande(new Date());

        //add commande to database
        Commande commandeCreated = commandeService.addCommande(commande);

        return new ResponseEntity<Commande> (commandeCreated , HttpStatus.CREATED);
     }


     @DeleteMapping(value = "/commandes/{id}")
     public void deleteCommande(@PathVariable Long id){

         commandeService
                 .findById(id)
                 .orElseThrow(()-> new ResourceNotFoundException("commande not found"));

        commandeService.deleteCommande(id);

     }

     @GetMapping(value = "/commandes/client/{id}")
    public List<Commande> commandesOfClient (@PathVariable Long id){
            return commandeService.getCommandesOfClient(id);
     }


}


