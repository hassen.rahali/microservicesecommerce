package com.monolith.demo.controller;

import com.monolith.demo.model.Commande;
import com.monolith.demo.model.Product;
import com.monolith.demo.repository.PaymentRepository;
import com.monolith.demo.service.PaymentService;
import com.monolith.demo.model.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

public class PaymentController {

    @Autowired
    PaymentService paymentService;
    @Autowired
    PaymentRepository paymentRepository;

    @GetMapping(value = "/payment/{commandeId}")
    public void  payerUneCommande(@PathVariable Long commandeId){
        paymentService.payerCommande(commandeId);
    }
    @GetMapping(value = "/testp")
    public List<Product> TestProduit()
    {
        return paymentService.TestProduit();
    }
    @GetMapping(value = "/testc")
    public List<Commande> TestCommande()
    {
        return paymentService.TestCommande();
    }

}
