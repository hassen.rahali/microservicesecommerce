package com.monolith.demo.service;

import com.monolith.demo.exceptions.CommandeNotFoundException;
import com.monolith.demo.model.Commande;
import com.monolith.demo.repository.CommandeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CommandeService {

    @Autowired
    CommandeRepository commandeRepository;


    public List<Commande> findAll(){
        return this.commandeRepository.findAll();
    }

    public Optional<Commande> findById(Long id) {

        Optional<Commande> commande = commandeRepository.findById(id);

        if (!commande.isPresent()) {throw new CommandeNotFoundException("Commande not found");}

        return commande ;
    }

    public Commande addCommande(Commande commande){
        return commandeRepository.save(commande);
    }

    public Commande updateCommande(Commande commande){
        return commandeRepository.save(commande);
    }

    public void deleteCommande (Long id){
         commandeRepository.deleteById(id);
    }

    public List<Commande> getCommandesOfClient(Long id){
     return  commandeRepository.findAll()
                .stream()
                .filter(commande -> commande.getIdClient().equals(id))
                .collect(Collectors.toList());
    }


}
