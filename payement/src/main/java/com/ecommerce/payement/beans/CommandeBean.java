package com.ecommerce.payement.beans;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

@Getter @Setter @NoArgsConstructor
@AllArgsConstructor @ToString
public class CommandeBean {
    private Long id;
    private Long idClient;
    private Long idProduct;
    private Date dateCommande ;
    private BigDecimal amount ;
    private int quantity ;
    private boolean isPayed;

}



