package com.ecommerce.payement.beans;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class ClientBean {
    private long id;
    private String name;
    private BigDecimal solde;
}
