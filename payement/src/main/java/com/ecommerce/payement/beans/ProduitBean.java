package com.ecommerce.payement.beans;

import lombok.*;

import java.math.BigDecimal;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProduitBean {
    private Long id ;
    private String name ;
    private BigDecimal price ;
    private int stockquantity;
}
