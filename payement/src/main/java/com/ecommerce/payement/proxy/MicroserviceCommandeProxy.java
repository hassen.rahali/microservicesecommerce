package com.ecommerce.payement.proxy;

import com.ecommerce.payement.beans.CommandeBean;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@FeignClient(name = "commandeMicroservice", url = "localhost:82")
public interface MicroserviceCommandeProxy {

    @GetMapping(value = "/commandes")
    public List<CommandeBean> allCommandes();

    @GetMapping(value = "/commandes/{id}")
    public ResponseEntity<CommandeBean> getCommande(@PathVariable Long id);


    @PutMapping(value = "/commandes")
    public ResponseEntity<CommandeBean> updateCommande(@RequestBody CommandeBean commande);



    @PostMapping(value = "/commandes" )
    public ResponseEntity<CommandeBean> passCommande (@RequestBody CommandeBean commande);


    @DeleteMapping(value = "/commande/{id}")
    public void deleteCommande(@PathVariable Long id);

}
