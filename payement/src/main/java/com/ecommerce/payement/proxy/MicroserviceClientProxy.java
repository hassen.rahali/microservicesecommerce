package com.ecommerce.payement.proxy;
import com.ecommerce.payement.beans.ClientBean;
import com.ecommerce.payement.beans.CommandeBean;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@FeignClient(name = "clientMicroservice", url = "localhost:8080")
public interface MicroserviceClientProxy  {
    @GetMapping(value = "/client/{id}")
    Optional<ClientBean> getClient(@PathVariable Long id);
    @PostMapping(value="/client")
    void addClient(@RequestBody ClientBean client);
}
