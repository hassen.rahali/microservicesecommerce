package com.ecommerce.payement.PaymentController;

import com.ecommerce.payement.PaymentRepository.PaymentRepository;
import com.ecommerce.payement.PaymentService.PaymentService;
import com.ecommerce.payement.beans.CommandeBean;
import com.ecommerce.payement.beans.ProduitBean;
import com.ecommerce.payement.model.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

public class PaymentController {

    @Autowired
    PaymentService paymentService;
    @Autowired
    PaymentRepository paymentRepository;

    @GetMapping(value = "/payment/{commandeId}")
    public void  payerUneCommande(@PathVariable Long commandeId){
        paymentService.payerCommande(commandeId);
    }
    @GetMapping(value = "/testp")
    public List<ProduitBean> TestProduit()
    {
        return paymentService.TestProduit();
    }
    @GetMapping(value = "/testc")
    public List<CommandeBean> TestCommande()
    {
        return paymentService.TestCommande();
    }

}
