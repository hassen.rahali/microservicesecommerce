#### **Product Microservice EndPoints**

These are the requests permited by this MicroService with a bief description.

| Path      |Type| Function                       |
|:--------------|:---|:----------------------------------|
| `/products`      | `GET`|returns a json array that contain all the products in the database
| `/product/{id}`    |`GET`| return the product with the Id=id in a json format
| `/addproduct` |`POST`  |Create a product with the json array passed in request body
| `/DeleteProduct/{id}`     | `DELETE`| Delete the product having an ID=id|

**_The application is running on port 81_**