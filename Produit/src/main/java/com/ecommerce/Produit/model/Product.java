package com.ecommerce.Produit.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
public class Product {
    @Id
    @GeneratedValue
    private Long id ;

    private String name ;

    private BigDecimal price ;

    private int stockquantity ;

    public Product() {

    }

    public Product(String name, BigDecimal price, int stockquantity) {
        this.name = name;
        this.price = price;
        this.stockquantity = stockquantity;
    }
}
