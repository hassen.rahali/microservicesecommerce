package com.ecommerce.Produit.controller;

import com.ecommerce.Produit.repository.ProductRepository;
import com.ecommerce.Produit.model.Product;
import javassist.NotFoundException;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/")
public class ProductController {
    @Autowired
    ProductRepository productRepository;
    @GetMapping(value="products/{id}")
    ResponseEntity<Product> getProduct (@PathVariable Long  id) throws ResourceNotFoundException {


       Product prod = productRepository
               .findById(id)
               .orElseThrow(()-> new ResponseStatusException(HttpStatus.UNAUTHORIZED,"product not found = "+id));

            return ResponseEntity.ok().body(prod);
    }



    @GetMapping(value="products")
    List<Product> getAllProducts () {

        List<Product> prod = productRepository.findAll();
        return prod;

    }

    @PostMapping(value = "products")
    ResponseEntity<Product> addproduct (@RequestBody Product prod){

        Product CreatedProduct = productRepository.save(prod);
        return ResponseEntity.ok().body(CreatedProduct);
    }

    @DeleteMapping(value="products/{id}")
    public void deleteProduct(@PathVariable Long id) throws  ResponseStatusException {

        Product prod = productRepository
                .findById(id)
                .orElseThrow(()-> new ResponseStatusException(HttpStatus.UNAUTHORIZED,"product not found = "+ id));
        productRepository.deleteById(id);


    }


}
