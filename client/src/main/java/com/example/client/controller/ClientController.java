package com.example.client.controller;


import com.example.client.beans.CommandeBean;
import com.example.client.beans.ProductBean;
import com.example.client.model.Client;
import com.example.client.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/")
public class ClientController {

    @Autowired
    ClientService clientService ;

@GetMapping(value = "clientPay/{idCommande}")
    void payer (Long idCommande){
    clientService.payerCommande (idCommande);
}

@GetMapping(value = "/client/{id}")
public  Client getClient (@PathVariable Long id){
    return clientService.getClient(id);
}

@GetMapping ( value = "commandes/{idClient}")
    List<CommandeBean> getAllCommandes (@PathVariable Long idClient){
    return  clientService.getAllCommandes(idClient);
}

@PostMapping(value = "client")
    Client addClient (@RequestBody Client client){

    return clientService.addClient(client);
}

@GetMapping(value = "products")
    List<ProductBean> getAllProducts() {
        return clientService.getAllProducts();
    }

@PostMapping(value = "PasserUneCommande")
    CommandeBean passerUneCommande(@RequestBody  CommandeBean comm){
    return clientService.passcommande(comm);
}

}



