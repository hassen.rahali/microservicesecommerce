package com.example.client.proxy;

import com.example.client.beans.ProductBean;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@FeignClient(name = "produitMicroservice", url = "localhost:81")
public interface ProductProxy {

    @GetMapping(value="products/{id}")
    ResponseEntity<ProductBean> getProduct (@PathVariable Long  id) throws ResourceNotFoundException;

    @GetMapping(value="products")
    List<ProductBean> getAllProducts ();

    @PostMapping(value = "products")
    ResponseEntity<ProductBean> addproduct (@RequestBody ProductBean prod);

    @GetMapping(value="products/{id}")
    void deleteProduct(@PathVariable Long id) throws ResponseStatusException;

}